<?php 

require "Database.php";

class Tipo extends Database {

	public function getTipos() {	
		$sql = 	"SELECT * FROM tipo ORDER BY descricao";
		$stmt = $this->db->prepare($sql);
		$stmt->execute();
		$tipos = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $tipos;
	}
}

?>