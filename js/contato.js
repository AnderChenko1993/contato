$(document).ready(function() {
	//Efetua requisição ajax na qual retorna os tipos de contatos
	$.ajax({
		url: "tipo_controller.php",
		type: "POST",
		dataType: "json",
		success: function(data) {
			for(i = 0; i < data.length; i++) {
				if(data[i].id == 1 || data[i].id == 2) 
					$("#tipo_email").append("<option value="+data[i].id+">"+data[i].descricao+"</option>");
				if(data[i].id != 1)
					$("#tipo_numero").append("<option value="+data[i].id+">"+data[i].descricao+"</option>");
			}
		},
		error: function (xhr, ajaxOptions, thrownError) {
			alert(xhr.status);
        	alert(thrownError);
		}
	});
	
	//Cria o campo dinâmico na ação do click do botão verde de add 
	$(".add-field-email").click(function() {
		$(".container-email").clone().attr('class', "col-md-5 container-email-clone").insertAfter(".container-email");
	});

	$(".add-field-telefone").click(function() {
		$(".container-telefone").clone().attr('class', "col-md-5 container-telefone-clone").insertAfter(".container-telefone");
	});

	//Cria máscara do campo número
	$(document).on("click", ".numero", function() {
		$(this).mask("(99) 99999-9999");	
	})
	
});