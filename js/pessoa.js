$(document).ready(function() {
	$.ajax({
		url: "pessoa_controller.php",
		type: "POST",
		dataType: "json",
		success: function(data) {
			for(i = 0; i < data.length; i++) {
				values = "<tr><td>"+data[i].id+"</td><td><img src="+data[i].caminho_imagem+" title="+data[i].nome+" class='foto-contato'></td><td>"+data[i].nome+"</td>";
				actions = "<td class='btn-actions'><span class='glyphicon glyphicon-search' value="+data[i].id+"></span></span><span class='glyphicon glyphicon-remove' value="+data[i].id+"></span></td></tr>";
				$(".table > tbody").append(values+actions);
			}
		},
		error: function (xhr, thrownError) {
			alert(xhr.status);
        	alert(thrownError);
		}
	});

	//EXIBE DADOS DO CONTATO EM UM MODAL
	$(document).on("mouseover", ".glyphicon-search", function() {
		$.ajax({
			url: "contato_controller.php",
			type: "POST",
			dataType: "json",
			data: {search_contato_pessoa: $(this).attr("value")},
			success: function(data) {
				$("#modal").empty();
				for(i = 0; i < data.length; i++) {
					$("#modal").append("<span><b>Contato:</b> "+data[i].contato+"<br/><b>Tipo:</b> "+data[i].tipo+"</span><br/><br/>");
				}

				$("#modal").dialog({
					title: "CONTATOS",
					width: 350,
					height: 400,
		        	modal: true,
		        	draggable: true,
		        	buttons: {
		        	"Fechar": function() {
		              		$(this).dialog("close");
		              	}
		          	}
				});
			},
			error: function (xhr, thrownError) {
				alert(xhr.status);
        		alert(thrownError);
			}
		});
	});
	

	//EXCLUI CONTATOS
	$(document).on("click", ".glyphicon-remove", function() {
		excluir = window.confirm("Deseja excluir este contato?");

		if(excluir) {
			$.ajax({
				url: "contato_controller.php",
				type: "POST",
				dataType: "json",
				data: {remove_pessoa: $(this).attr("value")},
				success: function(data) {
						alert("Contato excluído com sucesso!");
						location.reload(true);	
				},
				error: function (xhr, thrownError) {
					alert(xhr.status);
	        		alert(thrownError);
				}
			});	
		}
	});
});