<?php 

//Retira parenteses, traço e espaço vazio da string
function db_format($string) {
	$string = str_replace(["(", ")", "-", " "], "", $string);
	return $string;
}

function system_format($string) {
	$string = '(' . substr($string, 0, 2) . ') ' . substr($string, 2, 5) . '-' . substr($string, 7);
	return $string;
}

?>