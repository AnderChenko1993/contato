<?php 

require "Database.php";

class Pessoa extends Database {

	public function getPessoas() {
		$stmt = $this->db->prepare("SELECT * FROM pessoa ORDER BY nome, id");
		$stmt->execute();
		$pessoas = $stmt->fetchAll(PDO::FETCH_ASSOC);

		return $pessoas;
	}
}

?>