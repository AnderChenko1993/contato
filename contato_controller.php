<?php 

require "Contato.php";

$contato = new Contato();

if(isset($_POST['cadastro']) AND isset($_FILES)) {
	$salvo = $contato->salvar($_POST, $_FILES);

	if($salvo) {
		echo "<h3>CONTATO SALVO COM SUCESSO!</h3>";
		echo "<a href='index.html'>VOLTAR A TELA DE CONTATOS</a>";
	}
}

if(isset($_POST['search_contato_pessoa'])) {
	$contatos = $contato->getContatos($_POST['search_contato_pessoa']);
	echo json_encode($contatos);
}

if(isset($_POST['remove_pessoa'])) {
	$excluido = $contato->excluir($_POST['remove_pessoa']);

	if($excluido)
		echo json_encode(true);
	else
		echo json_encode(false);
}

?>