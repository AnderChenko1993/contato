<?php 

require "Database.php";
require "helpers.php";

class Contato extends Database {

	public function salvar($data, $imagem) {
		try {
			$this->db->beginTransaction();

			$caminho_imagem = $this->upload($imagem);

			//Salva pessoa
			$stmt = $this->db->prepare("INSERT INTO pessoa(nome, caminho_imagem) VALUES(:nome, :caminho_imagem)");
			$stmt->bindParam(':nome', $data['nome'], PDO::PARAM_STR);
			$stmt->bindParam(':caminho_imagem', $caminho_imagem, PDO::PARAM_STR);
			$stmt->execute();
			$pessoa =  $this->db->lastInsertId('id');

			//Salva contato(s) da pessoa
			for($i = 0; $i < count($data['descricao']); $i++) {
				$stmt = $this->db->prepare("INSERT INTO contato(id_pessoa, id_tipo_contato, descricao) VALUES(:id_pessoa, :id_tipo_contato, :descricao)");
				$stmt->bindParam(':id_pessoa', $pessoa, PDO::PARAM_INT);
				$stmt->bindParam(':id_tipo_contato', $data['tipo_contato'][$i], PDO::PARAM_INT);
				$descricao = db_format($data['descricao'][$i]);
				$stmt->bindParam(':descricao', $descricao, PDO::PARAM_STR);
				$stmt->execute();
			}
						
			$this->db->commit();
			return true;

		} catch(PDOException $e) {
			$this->db->rollback();
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}

	public function editar($id) {
		
	}

	public function excluir($id) {
		try {
			$this->db->beginTransaction();

			//Exclui contatos
			$stmt = $this->db->prepare("DELETE FROM contato WHERE id_pessoa = $id");
			$stmt->execute(); 	

			//Busca o caminho da imagem
			$stmt = $this->db->prepare("SELECT caminho_imagem FROM pessoa WHERE id = $id");
			$stmt->execute();

			//Exclui imagem do contato
			$imagem = $stmt->fetch(PDO::FETCH_ASSOC);
			unlink($imagem['caminho_imagem']);

			//Exclui pessoa
			$stmt = $this->db->prepare("DELETE FROM pessoa WHERE id = $id");
			$stmt->execute(); 				

			$this->db->commit();
			return true;
		} catch(PDOException $e) {
			$this->db->rollback();
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}

	//Busca os contatos referente a pessoa
	public function getContatos($id) {
		$stmt = $this->db->prepare("SELECT t.descricao AS tipo, c.descricao AS contato
									FROM contato AS c 
									INNER JOIN tipo AS t 
									ON c.id_tipo_contato = t.id
									WHERE id_pessoa = '".$id."'");
		$stmt->execute();
		$contatos = $stmt->fetchAll(PDO::FETCH_ASSOC);



		foreach($contatos as $key => $value) {
			if(is_numeric($contatos[$key]['contato'])) 
				$contatos[$key]['contato'] = system_format($contatos[$key]['contato']);
		}

		return $contatos;
	}

	public function upload($imagem) {
		//Definindo timezone padrão
		date_default_timezone_set("Brazil/East"); 

		//Atribuindo nome para a imagem
		$ext = strtolower(substr($_FILES['imagem']['name'], -4)); 
		$fileName = date("YmdHis") . $ext;

		//Efetuando o upload da imagem
		$caminho_imagem = 'uploads/'.$fileName;
		move_uploaded_file($_FILES['imagem']['tmp_name'], $caminho_imagem); 

		return $caminho_imagem;
	}

}

?>